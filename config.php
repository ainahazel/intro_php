<?php


ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

// set ENV so we know what database credentials to use 
// development or production 
// developemnt = local windows machine 
// when we want to go live, we go 

define('ENV', 'DEVELOPMENT'); 



//start a session 
session_start(); 


// if there are errors, get them out and assign them to a 
// simple varibale named 'errors'
// that's what 'isset' means : if errors isset, do this 

if(isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors']; 
    $_SESSION['errors'] = []; // clear old errors from session; 
} else {
    $errors = []; // so if there is errors, get it out
    // if there isn't any errors, just output an empty array
}

// if there are post values, get them out so we can use them easily
// get them out of the multi-dimensional array $_SESSION
// clear them so they don't hang around
if(isset($_SESSION['post'])) {
    $post = $_SESSION['post']; 
    $_SESSION['post'] = []; // clears old post values from $_SESSION
} else {
    $post = []; 
}




// Once this file is loaded, our porgram will have access to all the following constants 
// Constant is a value that cannot be changed after it is set 
// define the constant GST, set it's value to 0.5
define('GST', 0.5);

// define DB connection parameters


if(ENV ==='DEVELOPMENT') {
    define('DB_DSN', 'mysql:host=localhost;dbname=books');
    define('DB_USER', 'root'); 
    define('DB_PASS', '');
}

if(ENV === 'PRODUCTION') {
    define('DB_DSN', 'mysql:host=localhost;dbname=wdd12');
    define('DB_USER', 'wdd12'); 
    define('DB_PASS', 'rt98mn');
}




// resource/object 
// pdo is the database object
// setting it to these credentials 
$dbh = new PDO(DB_DSN, DB_USER, DB_PASS); 

// show errors - set error reporting
// takes two parameters 
// 1. whats the error mode attribute and setting it TOO 2. show exceptions 
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require 'functions.php';

//die('Dying here'); 
