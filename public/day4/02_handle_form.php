<?php 

require __DIR__ . "/../../config.php"; 

// 1. make sure its a POST REQUEST or die.. 
// look at the request method field 

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit('Unsupported request method.'); 
}

// if we get past this ^^ point, it IS a post request

// to satisfy our curiousity, dd dump
//dd($_POST); 

// 2. create an empty $errors array to hold our error messages 

$errors = []; 

// 3. Validate 
// super global $_POST ARRAY 
if(empty($_POST['name'])) {
    $errors['name'] = 'Publisher Name is a required field'; 
}

if(empty($_POST['city'])) {
    $errors['city'] = 'Pubisher City is a required field'; 
}

if(empty($_POST['phone'])) {
    $errors['phone'] = 'Pubisher Phone is a required field'; 
}

//dd($errors); 

// 4. now check if $error array is empty or not
// assign 'errors' to our $errors array and SAVE IT IN SESSION if there are errors
// outer array is the $_SESSION
// inner array is the $errors array 
// IF THERE ARE ERRORS, we wannt !!redirect!! to the form 
if(count($errors) > 0) {
    $_SESSION['errors'] = $errors; 
    $_SESSION['post'] = $_POST; 
    header("Location: 01_add_pub_form.php"); 
    die; // must die RIGHT !!AWAY!!
}

//echo 'We have no errors';




// create query 

$query = 'INSERT INTO publisher (
    name, 
    city, 
    phone
) VALUES (
    :name, 
    :city,
    :phone
)'; 


// prepare query 

$stmt = $dbh->prepare($query); 

// binding our parameters
// assignming values to the keys 

$params = array (
    ':name' => $_POST['name'],
    ':city' => $_POST['city'],
    ':phone' => $_POST['phone']
); 

//dd($params); 


// execute query 

$stmt -> execute($params); 

//die('Record inserted'); 


// IF INSERT WAS SUCCESSFUL ...
// 6. redirect to successful page 
// gives you the last ID for THIS CONNECTION
$publisher_id = $dbh->lastInsertId(); 


if($publisher_id > 0 ) {
    header('Location: 03_success.php?publisher_id=' . $publisher_id); 
    die; 
}
 

// test if the id is greater than zero 





//dd($stmt); 
//dd($dbh); 






