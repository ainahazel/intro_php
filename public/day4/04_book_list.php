<?php
/*
    Aina Ramos
    Intro to PHP
    2020-04-22
*/

require __DIR__ . "/../../config.php"; 


$title = "Books in Print"; 


$query =   'SELECT
            book.book_id,
            book.title,
            author.name as author, 
            book.year_published, 
            book.price, 
            genre.name as genre, 
            book.in_print
            FROM 
            book
            JOIN author USING(author_id)
            JOIN genre USING(genre_id)
            ORDER BY book.title ASC';

// because we don't have placeholders or parameters, 
// we don't need to prepare it 


$stmt = $dbh->query($query);

$stmt->execute(); 

$result = $stmt->fetchAll(); 




?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf8" />
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />

</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-sm-12">

            <h1><?=$title?></h1>

        </div>
    </div>

    <div class="row">


        <div class="col-sm-12">

            <table class="table table-striped">

                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Year Published</th>
                    <th>Price</th>
                    <th>Genre</th>
                </tr>


                <!-- Single Record Begins Here -->
                <?php foreach($result as $key => $value) : ?>
                <tr>
                    <td><a href="05_show_book.php?book_id=<?=$value['book_id']?>"><?=$value['title']?></a></td>
                    <td><?=$value['author']?></td>
                    <td><?=$value['year_published']?></td>
                    <td><?=$value['price']?></td>
                    <td><?=$value['genre']?></td>
                </tr>
                <!-- Single Record Ends Here -->
                <?php endforeach;?>
            </table>


        </div>


    </div>

</div>

</body>
</html>