# Day 4 Topics

* Setup Bitbucket for PHP Capstone

* Assignment 1
	- Conditions in Nav
	- COnditionals for CSS

* PRG - Finish sticky form
* PRG - Success Page

* Form to create publisher
* Handler validates
	- Connect to MySQL
    - Insert new publisher
    - redirect to success
    - Output new publisher

* MySQL List view
	- fetch and fetchAll
* MySQL Detail view
	- Static List View
* Connecting List and Detail
	- Dynamic Detail View
	- Validating the query var

* List and Edit
	- list view is the same
	- detail view is a form

* PHP Built In Validators
	- filter_var
		- validators



