<?php

// start a session
// this allows us to save data and access it on another page
// if that page has loaded this config file.
session_start();

// If there are errors, get them out so we can use them easily
if(isset($_SESSION['errors'])) {
    $errors =  $_SESSION['errors'];
    $_SESSION['errors'] = []; // clear old errors from session
} else {
    $errors = [];
}

// If there are post values, get them out so we can use them easily
if(isset($_SESSION['post'])) {
    $post =  $_SESSION['post'];
    $_SESSION['post'] = []; // clear old post values from session
} else {
    $post = [];
}


// Once this file is loaded, our program will have access
// to all the following constants

// Constant is a value that cannot be changed after it is set
// define the constant GST, set it's value to 0.5
define('GST', 0.5);

// Define DB connection parameters
define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');

require 'functions.php';
