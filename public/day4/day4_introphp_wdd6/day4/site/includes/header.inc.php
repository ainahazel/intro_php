<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$title?></title>

     <link rel="stylesheet" href="css/style.css" />


        <?php if($title == 'Shop') : ?>

            <link rel="stylesheet" href="css/shop.css" />

        <?php elseif($title == 'Support') : ?>

            <link rel="stylesheet" href="css/support.css" />

        <?php endif; ?>


     <?php if($title == 'About') : ?>

        <style>

            h1 {
                color: #900;
            }

        </style>

     <?php endif; ?>


    <style>

        a.current {
            padding: 5px;
            background: #000;
            color: #FFF;
            text-decoration: none;
        }
        
    </style>


</head>
<body>

<nav id="top">
    <?php require __DIR__ . '/nav.inc.php'; ?>
</nav>
