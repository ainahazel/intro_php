<ul>
        <!-- Set active state based on page $title -->
        <li><a <?=($title == 'Home') ? 'class="current"' : ''?> href="index.php">Home</a></li>
        <li><a <?=($title == 'About') ? 'class="current"' : ''?> href="about.php">About</a></li>
        <li><a <?=($title == 'Shop') ? 'class="current"' : ''?> href="shop.php">Shop</a></li>
        <li><a href="support.php">Support</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>