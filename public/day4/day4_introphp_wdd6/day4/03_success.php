<?php

require __DIR__ . '/../../config.php';

// If there's no publisher_id, die, because we need
// the publisher_id to select that record from the DB
if(empty($_GET['publisher_id'])) {
    die('Please insert a publisher to see this page');
}

// By binding values to placeholders, we escape the
// data that is being bound, and protect ourselves
// from SQL injection attacks.
$query = "SELECT *
            FROM publisher
            WHERE 
            publisher_id = :publisher_id";

$stmt = $dbh->prepare($query);

$params = array(
    ':publisher_id' => $_GET['publisher_id']
);

$stmt->execute($params);

$result = $stmt->fetch();

dd($result);

