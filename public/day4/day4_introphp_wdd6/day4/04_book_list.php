<?php

/*
Steve George
Intro to PHP
Quiz # 2
*/

require  __DIR__ . '/../../config.php';

$title = "Books in Print";

/*
<th>Title</th>
<th>Author</th>
<th>Year Published</th>
<th>Price</th>
<th>Genre</th>
<th>In Print</th>
*/

$query = 'SELECT 
          book.book_id,
          book.title,
          author.name as author,
          book.year_published,
          book.price,
          genre.name as genre,
          book.in_print
          FROM 
          book
          JOIN author USING(author_id)
          JOIN genre USING(genre_id)
          ORDER BY book.title ASC';

$stmt = $dbh->query($query);

$stmt->execute();

$result = $stmt->fetchAll();


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf8" />
    <title><?=$title?></title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <style>
        .container {
            max-width: 800px;
        }
    </style>

</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-sm-12">

            <h1><?=$title?></h1>

        </div>
    </div>

    <div class="row">


        <div class="col-sm-12">

            <table class="table table-striped">

                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Year Published</th>
                    <th>Price</th>
                    <th>Genre</th>
                    <th>In Print</th>
                </tr>

                <?php foreach($result as $row) : ?>

                    <?php if($row['in_print'] == 1) : ?>
                        <!-- Single Record Begins Here -->
                        <tr>
                            <td><a href="05_show_book.php?book_id=<?=$row['book_id']?>"><?=$row['title']?></a></td>
                            <td><?=$row['author']?></td>
                            <td><?=$row['year_published']?></td>
                            <td>$<?=$row['price']?></td>
                            <td><?=$row['genre']?></td>
                            <td><?=$row['in_print']?></td>
                        </tr>
                        <!-- Single Record Ends Here -->
                    <?php endif; ?>

                <?php endforeach; ?>

            </table>


        </div>


    </div>

</div>

</body>
</html>