<?php

require __DIR__ . '/../../config.php';

/*
Add a publisher
*/

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Add A Publisher</title>
</head>
<body>

    <h1>Add A Publisher</h1>

    <?php if(count($errors) > 0) : ?>
        <div class="errors">
            <ul>
                <?php foreach($errors as $error) : ?>
                <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <!--
        Best practice: field names in forms should match
        up with field names in your database table.
    -->
    <form action="02_handle_form.php" method="post" novalidate>
        <p><label for="name">Publisher Name</label><br />
            <input type="text" name="name" value="<?=old('name', $post)?>" /></p>
         <p><label for="city">Publisher City</label><br />
            <input type="text" name="city" value="<?=old('city', $post)?>" /></p>
         <p><label for="phone">Publisher Phone</label><br />
            <input type="text" name="phone" value="<?=old('phone', $post)?>" /></p>
        <p><button type="submit">Submit</button></p>
    </form>

</body>
</html>