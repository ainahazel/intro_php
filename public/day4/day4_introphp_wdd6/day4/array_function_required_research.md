# Array Functions Required Research

There are number of PHP array functions you should be familiar with, that we may not cover in detail in class.  You can find out about them here:

[PHP Array Functions](https://www.php.net/manual/en/ref.array.php)

Functions you should be familiar with include (but are not limited to):

* array_pop
* array_push
* array_shift
* array_unshift
* array_filter
* array_merge
* in_array
* array_key_exists
* count
* size_of
* sort
* rsort
* asort
* arsort
* ksort
* krsort
* shuffle
* compact
* extract
* range



