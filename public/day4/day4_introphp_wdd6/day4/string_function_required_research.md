# Required Research

## PHP String Functions

[String Functions Reference](https://www.php.net/manual/en/ref.strings.php)

Functions you should be familiar with include (at a very minimum):

* strlen
* strpos
* str_replace
* strip_tags
* ucfirst
* ucwords
* strtoupper
* strtolower
* htmlentities
* html_entity_decode
* htmlspecialchars
* htmlspecialchars_decode
* explode
* implode

As well, you should be familiar with the MultiByte versions of all of these, and understand the difference between the standard string functions and the multibyte versions.

[MultiByte String Functions Reference](https://www.php.net/manual/en/ref.mbstring.php)

