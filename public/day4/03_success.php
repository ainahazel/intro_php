<?php

require __DIR__ . "/../../config.php"; 

// if there's no publisher id, die 
// because we need a pub id 
// to select that record from the DB 

// we're stopping users from entering the page 
// unless there is a publisher id 

if(empty($_GET['publisher_id'])) {
    die('Please insert a publisher to see this page'); 
}


// make a query to GET AND OUTPUT info from table publisher 


// by binding values to placeholders, 
// we escape the data that is being bound, 
// and protect ourselves from 
// SQL injection attacks 
$query =    "SELECT * 
            FROM publisher
            WHERE
            publisher_id = :publisher_id";

$stmt = $dbh->prepare($query); 

$params = array (
    ':publisher_id' => $_GET['publisher_id']
); 


$stmt->execute($params);

$result = $stmt->fetch(); 


dd($result); 


