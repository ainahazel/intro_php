<?php 

require '../../config.php';

// create some variables 
 

$num = 16; // integer

$num2 = 12.999;  // float 
    
$name = "Aina Ramos"; // string

$fruits = array('apple', 'orange', 'banana', 'peach'); // ordinal array 

$user = array(
    'first' => 'Happy', 
    'second' => 'Gilmore', 
    'age' => 12, 
    'hobbies' => array('reading', 'cycling', 'movies') 
); // associative array 

// var_dump : breakdown of the varibale including the datatype of the variables in that element 

// print_r : gives us a simple structure of a variable 

// var_export : is the one that gives you the exact layout you would want to use to restructure your variable 


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
   
   
   <h>Basic Debuggin</h>
   
   <pre>
     
    <p> <?php print_r($fruits);?> </p>
     
    <p> <?php var_dump($fruits);?> </p> 
     
    <p> <?php print_r($user);?> </p>
    
    <p> <?php var_export($user);?> </p>
     

    </pre>
   
   
   
   
   
   
   
   
   
  </body>
</html>