<?php 

require '../../config.php'; 

// all variables in PHP start with a dollar sign 
// you don't need to declare var or let 
// you can use a single or double quotes - just be consistent 


$title = "Variables and Constants"; 


// if we use double quotes for strings, 
// variables and special characters used 
// within the string, will be interpolated 

// interpolated : turns variables into their values 

$name = "Aina"; 


$para = "Hello, my name is $name"; 


$para2 = 'Hello, my name is $name'; 


$para3 = "  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, harum esse molestias ratione quasi modi. Voluptatibus quidem tempora vitae, vel modi, officiis fuga quisquam rerum laboriosam quibusdam aperiam aliquam nisi.
";                                    



// to define a contant, we use the keyword 'define'
//define('GST', 0.5);



?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
      <style>
      </style>
  </head>
    
  <body>  
  
  <h1><?php echo $title; ?></h1>
  
  <p><?php  echo $para;    ?></p>
  
  <p><?php  echo $para2;    ?></p>
  
  <p><?php  echo $para3;    ?></p>
  
  <p><?=$para3?></p>
  
  
  <p>Manitobas GST is <?=$GST?> </p>
  
  
  <p></p>
  
  
  </body>
</html>