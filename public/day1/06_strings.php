<?php
    

/* two string:
    
    1. single quotes = string literals 
    
    2. double quotes = interpolated strings
 
*/ 


// Ex. 

    $title = "AINAHAZELRAMOS"; 

    // LITERAL : this won't get converted to the value of the variable title
    $string1 = 'the title of this page is $title'; 

    // INTERPOLATED : will give me the value of the variable 
    $string2 = "The title of this page is $title"; 




/*
    1. HEREDOC = equivalent of a literal string
    
    2. NOWDOC = equivalent of an interpolated string 
    
*/



    // this ones come out exactly like it looks 
    // foobar is just a 'marker' to tell you when it starts and ends 
$string3 = <<<'FOOBAR'

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates blanditiis, molestias hic ducimus odit!</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates bla <b>{$title}</b >nditiis, molestias hic ducimus odit!</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates blanditiis, molestias hic ducimus odit!</p>


FOOBAR; 


$string4 = <<<FOOBAR

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates blanditiis, molestias hic ducimus odit!</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates bla <b>{$title}</b >nditiis, molestias hic ducimus odit!</p>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam voluptate debitis ut voluptatem distinctio reprehenderit repellendus tenetur sed, error labore dicta maiores deleniti! Inventore voluptates blanditiis, molestias hic ducimus odit!</p>


FOOBAR; 



?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
        
    <h3>Nowdoc</h3>
    <p><?=$string3?></p> 
    
    <h3>heredoc</h3> 
    <p><?=$string4?></p>  
    
        
            
                
                        
  </body>
</html>