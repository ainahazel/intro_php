<?php 
echo "<h1>Printing above the Doctype</h1>"; 

echo "<p>This is a paragraph outputted by php</p>\n"; 

echo 5; 

echo "<br> \n"; 

echo 5 + 6 ; 

// we can concatenate 

echo "<p>Hello" . " " . "World</p> \n"; 


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
   
   
   <?php
    
    echo "-------------------------------------------------------------------------------------------------------------------------------------<br>"; 
      
    echo "<em>printed in the body</em>"; 
    
    echo "<h2>generating output</h2>"; 
      
    print("<p>This line is generated with print() function </p>"); 
      
    printf("<p>This is the first %d and this is the second %d </p>", 3,20); 
      
    
      
    
   ?>
  </body>
</html>