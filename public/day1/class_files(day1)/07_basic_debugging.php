<?php

require '../../config.php';

// Create some variables


$num = 16;  // integer

$num2 = 22.39; // float

$name = "Happy Gilmore"; // string

$fruits = array('apple', 'orange', 'banana', 'peach'); // array

$user = array(
    'first' => 'Happy',
    'last' => 'Gilmore',
    'age' => 12,
    'hobbies' => array('reading', 'cycling', 'movies')
); // associative array

// var_dump -- breakdown, including data type of the elements

// print_r -- simple structure of variable

// var_export -- prints actual code that can be used to recreate variable

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Basic Debugginf</title>
</head>
<body>

    <h1>Basic Debugging</h1>

<pre>

<?php print_r($fruits); ?>

<?php var_dump($fruits); ?>

<?php print_r($user); ?>

<?php var_dump($user); ?>

<?php var_export($user); ?>

<?php

$test = array (
  'first' => 'Happy',
  'last' => 'Gilmore',
  'age' => 12,
  'hobbies' => 
  array (
    0 => 'reading',
    1 => 'cycling',
    2 => 'movies',
  ),
);



?>


</pre>

</body>
</html>


