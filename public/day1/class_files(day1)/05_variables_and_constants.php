<?php

// Use the 'include' keyword, if the file you are
// trying to import is not CRITICAL to your site's
// operation.  You will get a warning if the file
// cannot be found, but the rest of the page will load
// include '../../config.php';

// Use the 'require' keyword, if the file you are
// trying to import is CRITICAL to your site's operation.
// You will get a fatal error if the file is not found,
// and the page will stop loading at the point of the error
require '../../config.php';

// variables with PHP start with a $
$title = "Variables and Constants";

$name = "Steve";

// If we use double quotes for string,
// variables and special charactes used
// within the string, will be interpolated
$para = "Hello, my name is $name.";

// If we use single quotes for string,
// variables and special characters
// are NOT interporalted, they are treated
// as literal strings.
$para2 = 'Hello, my name is $name.';

$para3 = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

// Only acceptable characters: undrscore, letters, numbers
// All variables must beging with dollar
// followed by letters or an underscore
// followed by letters, numbers, or underscores

// $abc_abd  
// $_abc123
// $1abc   // illegal cannot start variable name with a number
// $_12343
// Variables are case sensitive
// $Name is different from $name or $naMe


?><!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8" />
</head>
<body>

    <h1><?php echo $title; ?></h1>

    <p><?php echo $para; ?></p>

    <p><?php echo $para2; ?></p>

    <!-- Nice succinct echo syntax for template -->
    <p><?=$para3?></p>

    <h3>Constants defined in config file</h3>

    <p>The value of Manitoba's GST is <?=GST?></p>

    <p>The database username is: <?=DB_USER?></p>

    <p>The database password is: <?=DB_PASS?></p>

    <?php

        // This will not work, as we already defined
        // this constant... constants cannot be changed
        // once defined.
        define('GST', .06);

    ?>

</body>
</html>


