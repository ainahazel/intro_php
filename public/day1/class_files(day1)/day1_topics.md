# Day 1 Topics

-- Background
    -- Rasmus Lerdorf
    -- PHP ... acronym, now and the past
    -- PHP created specifically for Web Development

-- Setting a Vhost (Virtual host) on XAMPP
    -- httpd-vhosts.conf
    -- windows host file

-- PHP Built in Server
    -- fast testing
    -- if you can't get XAMPP

-- PHP tags
    - open: <?php
    - closing ?>
    - Do not using closing tag unless in HTML file

-- Generating Output
    - echo
    - print
        - echo and print are interchangable
    - printf() - formatted printing
    - Alternative syntax for templates:  <?=$var?>

-- Comments
    - `// single line`
    - `/* mutli-line */`
    - `# Bash type comments`

-- Variables
    - All variables begin with $
    - `Can only contain _, letters, numbers`
    - Must begin with `_ or letters`

-- Constants
    - Do not require $
    - set using define('CONSTANT_NAME', 'value');
        - define('GST', 0.05)
        - define('SITE_TITLE', 'Learning PHP');
    - No need to use $ when accessing constants:
        - echo GST;

-- Strings
    - String literals, contained in single quotes
    - Interpolated strings, contained in double quotes
    - In interpolated strings, variable names are replaced by their values

-- Long Strings (very long HTML blocks)
    - NOWDOC - equivalent of literal string
        - `$string = <<<'EOT'`
    - HEREDOC - equivalent of interporated string
        - `$string = <<<EOT`

-- Debugging
    - Use `var_dump` for detailed debug of variable
    - Use `print_r` for structure of variable
    - Use `var_export` for code to recreate variable

-- Complex variables
    - arrays 
        - $fruits = array('apple', 'orange', 'banana');
        - $fruits = ['apple', 'orange', 'banana'];
    - associative arrays
        
```php

    $user = (
        'first' => 'Dave',
        'last' => 'Jones',
        'age' => 22,
        'email' => 'djones@example.com'
    );

```

-- Includes and Requires
    - `include 'filename.php'` - gives warning if file not found
    - `require 'filename.php'` - fatal error if file not found

