<?php

require '../../config.php';

// Create some variables

$num = 16;  // integer

$num2 = 22.39; // float

$name = "Happy Gilmore"; // string

$fruits = array('apple', 'orange', 'banana', 'peach'); // array

$user = array(
    'first' => 'Happy',
    'last' => 'Gilmore',
    'age' => 12,
    'hobbies' => array('reading', 'cycling', 'movies')
); // associative array

// var_dump -- breakdown, including data type of the elements

// print_r -- simple structure of variable

// var_export -- prints actual code that can be used to recreate variable

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Debugging with dd</title>
</head>
<body>

<h1>Debugging with dd()</h1>

<?php

    dd($num); // will die here

    dd($num2);

    dd($name);

    dd($fruits);

    dd($user);

?>

</body>
</html>