
<?php
// Blocks of PHP must begin with opening PHP tags



// PHP tags embedded in HTML, like these, must close with 
// closing PHP tags
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Php Tags</title>
</head>
<body>

	<h1>PHP tags</h1>

	<?php

		// PHP  tags embedded in HTML must have 
		// both opening and closing tags

		echo "<h2>Hello, World!</h2>";

	?>

	<p>This is normal html.</p>

</body>
</html>