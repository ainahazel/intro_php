<?php

/*
This is a file about comments
date: 2020-04-17
 */

// Outputting hello
echo "Hello";

echo /* echoing goodbye */ "goodbye";

print(5+6); // 11

// Test for value of X
if($x) {
	// do something
} else {
    // do something else
}

// C comments

# C comments

/* 
Multi line comments 
*/



/**
 * This is my function
 * We use DOCBLOCK comments for functions
 */
function()
{
    // do something
}

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title></title>
</head>
<body>

	<!-- HTML comments are separate from PHP comments -->

	<?php
	// echoing something
		echo "Something";
	?>

</body>
</html>

