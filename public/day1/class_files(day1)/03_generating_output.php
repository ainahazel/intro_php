<?php

// We can output numbers, strings, expressions
// anytihing that can be displayed in HTML.

echo "<h1>Printed above the DOCTYPE</h1>";

// echo is the most common way to print data
echo "<p>This is a paragraph output by PHP</p>\n";

// echo can output simple values
echo 5;

// echo can output html
echo "\n<br />\n";

// echo can output expressions
echo 5 + 6; // 11


// we can concatentate with php
// The concatenation operator is . in PHP
echo "<p>Hello" . " " . "World!</p>";

// Everything above here will be output ABOVE the DOCTYPE
// Which means your HTML will not validate
// 
// 
// In a real, NEVER output above the DOCTYPE.

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Generating Output</title>
</head>
<body>

	<h1>Printed in the body</h1>

	<?php

		// echo can be used anywhere in a PHP file
		echo "<h2>Generating output</h2>";

		// print works exactly like echo, except it's a function
		print("<p>This line generated with print() function.</p>");

		// Formatted output
		printf("<p>This number: %d, was output with printf().  
			This number %d, too.</p>", 1, 6);

	?>

	<p>This is being output OUTSIDE of PHP.</p>

	<!-- Echoed content on a single line with opening and closing tags -->
	<?php echo "<h3>A subhead</h3>"; ?>

	<script>

		document.write('<p>Output by Javascript</p>');

	</script>

	<p>Output by HTML</p>

	<script>

		document.write('<p>Output by Javascript</p>');

	</script>


</body>
</html>




