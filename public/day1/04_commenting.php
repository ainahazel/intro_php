<?php 

/* 
    You usually put a multi line comment at the very top 
    date 2020-04-17

*/


// this is a regular comment


echo /* gna echo goodbye */ "goodbye"; 


    
# you can also use a hash as a comment 


// C comments are the ones in between functions 

if($x) {
    
} else {
    
}

?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
    <header>
        
    </header>
      
  </body>
</html>