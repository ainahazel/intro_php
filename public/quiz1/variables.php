<?php

/**
 * Variables for use in Quiz 1
 */

$title = "Quiz #1 - Modern Spacecraft";

$subtitle = "Exploring space one parsec at a time!";

$img1 = "columbia.jpg";

$img2 = "soyuez.jpg";

$img3 = "orion.jpg";

$orion = "NASA’s Orion spacecraft will carry astronauts further into space than ever before using a module based on Europe’s Automated Transfer Vehicles (ATV). The ATV-derived service module, sitting directly below Orion’s crew capsule, will provide propulsion, power, thermal control, as well as supplying water and gas to the astronauts in the habitable module. The first Orion mission will be an uncrewed lunar flyby in 2017, returning to Earth’s atmosphere at 11 km/s ­– the fastest reentry ever.";

$caption = "More than 100 Soviet and Russian crewed Soyuz spacecraft have flown since 1967";

$columbia = "The US Space Shuttle flew 135 times from 1981 to 2011, supporting Spacelab, Mir, the Hubble Space Telescope, and the ISS. The April 12 launch at Pad 39A of STS-1, just seconds past 7 a.m., carries astronauts John Young and Robert Crippen into an Earth orbital mission scheduled to last for 54 hours, ending with unpowered landing at Edwards Air Force Base in California.";


$content = <<<EOT

<p>Space exploration is the use of astronomy and space technology to explore outer space. While the study of space is carried out mainly by astronomers with telescopes, its physical exploration though is conducted both by unmanned robotic space probes and human spaceflight.</p>

<p>While the observation of objects in space, known as astronomy, predates reliable recorded history, it was the development of large and relatively efficient rockets during the mid-twentieth century that allowed physical space exploration to become a reality. Common rationales for exploring space include advancing scientific research, national prestige, uniting different nations, ensuring the future survival of humanity, and developing military and strategic advantages against other countries.</p>

EOT;
