
<?php
/* 

    Aina 
    Introduction to PHP - Quiz 1 
    2020-04-20
    
*/

require 'variables.php'; 


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf8" />
   
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />

    <style>
        img {
            width: 100%;
            max-width: 100%;
            height: auto;
            margin-bottom: 15px;
        }
        .row {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    
    <div class="container">

        <div class="row">
            <div class="col-sm-12 text-center">

                <!-- Title here -->
                <h1> <?php echo $title; ?> </h1>

                <!-- Soyuez picture here -->
                <img src="images/<?=$img2?>" alt="Soyuez" />
            </div>
            <div class="col caption text-center">
                <!-- Caption here -->
                <?=$caption?>
                <p></p>
            </div>
        </div><!-- /.row -->

        <div class="row">

            <div class="col col-sm-6">

                <!-- Columbia picture here -->
                <img src="images/<?php print($img1); ?>" alt="Columbia" />

                <div class="lead">
                    <!-- Columbia paragraph here -->
                    
                    <p><?php print($columbia); ?></p>
                    
                </div>

            </div><!-- /.columbia -->

            <div class="col col-sm-6">

                <!-- Orion picture here -->
                <img src="images/<?=$img3?>" alt="Orion" />

                <div class="lead">
                    <!-- Orion paragraph here -->
                    <p>
                        <?php echo $orion; ?>
                    </p>
                </div>

            </div><!-- /.orion -->

        </div><!-- /.row -->

        <div class="row">

            <div class="col col-xs-12">

                <!-- subtitle here -->
                <h2 class="text-center"><?=$subtitle?></h2>
                
                <!-- Main content here -->
                <?= print($content); ?>

            </div><!-- /.main-content -->

        </div><!-- /.row -->


    </div>

</body>
</html>