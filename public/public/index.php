<?php

require __DIR__ . '/../../../config.php'; 

/*
    Aina 
    Practical Exam 
    2020-04-27

*/



// Make sure that a book_id is set in the query string
if(empty($_GET['book_id'])) {
    die('book_id is required'); 
}
// connect to MySQL using PDO.  Be sure to use the `booksite` database 


// You will need to set your DB_DSN, DB_USER, DB_PASS constants or copy
// then over from your config.file
define('DB_DSN', 'mysql:host=localhost;dbname=booksite'); 
define('DB_USER', 'root'); 
define('DB_PASS', '');

//$dbh = new PDO(DB_DSN, DB_USER, DB_PASS); 

// Make sure MySQL is set to show errors
$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 

// Create the query that will generate the sample result
$query = 'SELECT 
            book.book_id, 
            book.description, 
            book.title, 
            author.name as author, 
            genre.name as genre, 
            book.year_published, 
            book.num_pages, 
            book.price, 
            publisher.name as publisher, 
            format.name as format, 
            book.in_print
            FROM 
            book
            JOIN author using(author_id)
            JOIN genre using(genre_id)
            JOIN publisher using(publisher_id)
            JOIN format using(format_id)
            WHERE book_id = :book_id'; 

// prepare the query

$stmt = $dbh->prepare($query); 

// create your params array 
$params = array(
    'book_id' => intval($_GET['book_id'])
); 


// execute the query 

$stmt->execute($params); 


// get your result
$result = $stmt->fetch(PDO::FETCH_ASSOC); 


// add function to escape output properly
esc($result);

?><!DOCTYPE html>
<html>
<head>

    <!-- Make sure any required variables are output in the head -->

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale:1.0" />
    <title><?=$book['title']?></title>
    <style>

        img {
            width: 100px;
            height: auto;
        }

        table {
            border-collapse: collapse;
        }

        td, th {
            border: 1px solid #cfcfcf;
            padding: 8px;
            text-align: left;
        }

    </style>
</head>
<body>

    <!-- 
    Output body content to match the sample result
    including the page title and all database content.
    Make sure all data is output according to best practices
    -->
    <h1><?=$book['title']?></h1>
    
    <table>
       <tr>
           <td><strong>Cover:</strong></td>
           <td><!-- an image goes here--></td>
       </tr>
       
        <tr>
           <td><strong>Description: </strong></td>
           <td><?=$book['description']?></td>
       </tr>
       
        <tr>
           <td><strong>Title: </strong></td>
           <td><?=$book['title']?></td>
       </tr>
       
        <tr>
           <td><strong>Author:</strong></td>
           <td><?=$book['author']?></td>
       </tr>
       
        <tr>
           <td><strong>Genre: </strong></td>
           <td><?=$book['genre']?></td>
       </tr>
       
       <tr>
           <td><strong>Year Published: </strong></td>
           <td><?=$book['year_published']?></td>
       </tr>
       
       <tr>
           <td><strong>Number of Pages: </strong></td>
           <td><?=$book['num_pages']?></td>
       </tr>
       
       <tr>
           <td><strong>Price: </strong></td>
           <td><?=$book['price']?></td>
       </tr>
       
       <tr>
           <td><strong>Publisher: </strong></td>
           <td><?=$book['publisher']?></td>
       </tr>
       
       <tr>
           <td><strong>Format: </strong></td>
           <td><?=$book['format']?></td>
       </tr>
       
       <tr>
           <td><strong>In Print: </strong></td>
           <td><?=$book['in_print']?></td>
       </tr>
        
    </table>

    
    

    
</body>
</html>