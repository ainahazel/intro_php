# Day 5 Topics

* Quiz Review

* PHP Validation Filters
    - filter_var w/ FILTER_VALIDATE_EMAIL

* Other Validation Techniques
    - is_numeric() -- test if value is a number
    - strlen() or mb_strlen() -- test length of string
    - count() or sizeof() -- test size of array
    - empty() -- test if variable is declared and contains data
    - isset() -- test if variable has been declared

* PHP MySQL Insert (PRG)
    - The detail view

* Importance of List and Details views

* PHP MySQL List & Detail views
    - Connecting list an detail
    - The detail view

* PHP MySQL List and Edit views
    - The edit detail view
    - Edit PRG - success is list view

* Multi List View 
    - Page composed of multiple queries
    - Use new book.sql and booksite html


* Review

* Practical Quiz
