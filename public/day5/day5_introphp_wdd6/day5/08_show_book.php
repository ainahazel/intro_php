<?php

require __DIR__ . '/../../config.php';

if(empty($_GET['book_id'])) {
    die('Book_id required');
}

// 1. Write Query

$query = 'SELECT 
        book.book_id,
        book.title,
        book.num_pages,
        book.in_print,
        book.price,
        author.name as author,
        author.country as author_country,
        publisher.name as publisher,
        publisher.city as publisher_city,
        publisher.phone as publisher_phone,
        genre.name as genre,
        format.name as format
        FROM 
        book 
        JOIN author USING(author_id)
        JOIN publisher USING(publisher_id)
        JOIN genre USING(genre_id)
        JOIN format USING(format_id)
        WHERE book_id = :book_id';

// 2.  Prepare Query
$stmt = $dbh->prepare($query);

$params = array(
    ':book_id' => intval( $_GET['book_id'] ) // 13ddd = 13  aad = 0
);

// 3. Execute Query
$stmt->execute($params);

// 4. Fetch results
$book = $stmt->fetch(PDO::FETCH_ASSOC);


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=esc( $book['title'] )?></title>
</head>
<body>

    <h1>Book Detail: <?=esc( $book['title'] )?></h1>

    <ul>
    <?php foreach($book as $key => $value) : ?>

        <?php if($key != 'book_id') : ?>
            <li><strong><?=esc( label($key) )?></strong>: <?=esc( $value )?></li>
        <?php endif; ?>

    <?php endforeach; ?>
    </ul>

</body>
</html>
