<?php

/**
 * Handle Add Publisher Form
 */

require __DIR__ . '/../../config.php';

// If not a POST request, die with an error message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// Validate all fields

// Set empty errors array
$errors = [];

if(empty($_POST['name'])) {
    $errors['name'] = 'Publisher Name is a required field';
}

if(empty($_POST['city'])) {
    $errors['city'] = 'Publisher City is a required field';
}

if(empty($_POST['phone'])) {
    $errors['phone'] = 'Publisher Phone is a required field';
}


if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    $_SESSION['post'] = $_POST;
    header('Location: 04_add_publisher_form.php');
    die;
}

// If all is good, insert record into database

$query = 'INSERT INTO publisher 
            (name, city, phone)
            VALUES 
            (:name, :city, :phone)';

$stmt = $dbh->prepare($query);

$params = array(
    ':name' => $_POST['name'],
    ':city' => $_POST['city'],
    ':phone' => $_POST['phone']
);

$stmt->execute($params);

$publisher_id = $dbh->lastInsertId();

if($publisher_id > 0) {
    header('Location: 06_success.php?publisher_id=' . $publisher_id);
    die;
}