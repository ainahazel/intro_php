<?php

$name = '';

$email = 'edu@uwinnipeg.ca';

// $postal_code = '';


echo '<h2>isset</h2>';

if(isset($name)) {
    echo '<p>Name is set, and has an empty value</p>';
} else {
    echo '<p>Name is NOT set, and has an empty value</p>';
}

if(isset($email)) {
    echo '<p>Email is set, and has a value</p>';
} else {
    echo '<p>Email is NOT set, and has a value</p>';
}

if(isset($postal_code)) {
    echo '<p>Postal code is set, and has no value</p>';
} else {
    echo '<p>Postal code is NOT set, and has no value</p>';
}


echo '<h2>empty</h2>';

if(empty($name)) {
    echo '<p>Name is not set or has no value</p>';
} else {
    echo '<p>Name is set, and has a value</p>';
}

if(empty($email)) {
    echo '<p>Email is not set, or has no value</p>';
} else {
    echo '<p>Email is set, and has a value</p>';
}

if(empty($postal_code)) {
    echo '<p>Postal code is not set, or has no value</p>';
} else {
    echo '<p>Postal code is set, and has a value</p>';
}


show_source(__FILE__);