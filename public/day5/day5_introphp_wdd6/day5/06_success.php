<?php

require __DIR__ . '/../../config.php';

if(empty($_GET['publisher_id'])) {
    die('Please use form to add a new publisher');
}

$query = "SELECT *
            FROM publisher 
            WHERE 
            publisher_id = :publisher_id";

$stmt = $dbh->prepare($query);

$params = array(
    ':publisher_id' => intval($_GET['publisher_id'])
);

$stmt->execute($params);

$result = $stmt->fetch(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$result['name']?></title>
</head>
<body>

    <h1>You added the following publisher</h1>

    <ul>
        <li><strong>Publisher ID</strong>: <?=$result['publisher_id']?></li>
        <li><strong>Publisher Name</strong>: <?=$result['name']?></li>
        <li><strong>Publisher City</strong>: <?=$result['city']?></li>
        <li><strong>Publisher Phone</strong>: <?=$result['phone']?></li>
    </ul>

    <p><a href="04_add_publisher_form.php">Add another publisher</a></p>

</body>
</html>