<?php

require __DIR__ . '/../../config.php';

dd($_POST);

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Form Array</title>
</head>
<body>

    <h1>HTML Form Array</h1>

    <form action="02_html_form_array.php" method="post">
        <p><label for="name">Name</label>: 
            <input type="text" name="name" /></p>
        <p><label for="siblings">Siblings</label><br />
            <input type="checkbox" name="siblings[]" value="sister" /> sister 
            <input type="checkbox" name="siblings[]" value="brother" /> brother  
            <input type="checkbox" name="siblings[]" value="other" /> other 
        </p>
        <p><button type="submit">Submit</button>
    </form>

</body>
</html>