<?php

// filter_var


$email1 = 'steve@glort.com';

$email2 = 'sadfasdf';


// This will return 1 of 2 things:
// 1. if valid, it will return the email address
// 2. if not valid, returns false
// filter_var($email1, FILTER_VALIDATE_EMAIL);

if(filter_var($email1, FILTER_VALIDATE_EMAIL) === $email1) {
    echo $email1 . ' is a valid email<br />';
} else {
    echo $email1 . ' is an invalid email<br />';
}

if(filter_var($email2, FILTER_VALIDATE_EMAIL) === $email2) {
    echo $email2 . ' is a valid email<br />';
} else {
    echo $email2 . ' is an invalid <br />';
}

$num1 = '22';
$num2 = 'dd';

echo '<h2>Using filter_var FILTER_VALIDATE_INT</h2>';

if(filter_var($num1, FILTER_VALIDATE_INT) === $num1) {
    echo "'" . $num1 . "'" . ' is a valid integer<br />';
} else {
    echo "'" . $num1 . "'" . ' is not a valid integer<br />';
}


echo '<h2>Using is_numeric()</h2>';
// Use this for numbers
if(is_numeric($num1)) {
    echo "'" . $num1 . "'" . ' is a valid number<br />';
} else {
    echo "'" . $num1 . "'" . ' is not a valid number<br />';
}

/*
$name1 = "Ed";
$name2 = "Randy";

if(strlen($name1) < 2) {
    echo 'Name must have at least 2 characters'
} */




