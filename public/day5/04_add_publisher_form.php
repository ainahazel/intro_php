<?php

require __DIR__ . '/../../config.php';

/*
Add a publisher
*/

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Add A Publisher</title>
</head>
<body>

    <h1>Add A Publisher</h1>
    
    <!-- This is where we're going to display the errors IF there are errors -->
    <?php if(count($errors) > 0) : ?>
        <div class="errors">
            <ul>
                <?php foreach($errors as $error) : ?>
                <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <!--
        Best practice: field names in forms should match
        up with field names in your database table.
    -->
    
    <!-- The old function is what makes sure that the form is sticky! If rhw field is not empty, then we wanr to return it back to the form so that the user doesn't have to re-write everything-->
    
    <!-- Aina, don't forget to add the escaping output function to make it secure and no sql injections can be done  -->
    <form action="05_handle_form.php" method="post" novalidate>
        <p><label for="name">Publisher Name</label><br />
            <input type="text" name="name" value="<?=esc(old('name', $post))?>" /></p>
         <p><label for="city">Publisher City</label><br />
            <input type="text" name="city" value="<?=esc(old('city', $post))?>" /></p>
         <p><label for="phone">Publisher Phone</label><br />
            <input type="text" name="phone" value="<?=esc(old('phone', $post))?>" /></p>
        <p><button type="submit">Submit</button></p>
    </form>

</body>
</html>