<?php

require __DIR__ . '/../../config.php';

//dd($_GET);


// we don't wanna be here UNLESS there is a book_id identified
if(empty($_GET['book_id'])) {
    die('book_id is required'); 
}



// 1. make query
$query = 'SELECT 
book.book_id, 
book.title, 
book.num_pages, 
book.in_print, 
book.price, 
author.name as author, 
author.country as author_country, 
publisher.name as publisher, 
publisher.city as publisher_city, 
publisher.phone as publisher_phone, 
genre.name as genre, 
format.name as format
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN genre USING(genre_id)
JOIN format USING(format_id) 
WHERE book_id = :book_id'; 


// 2. prepare query
$stmt = $dbh->prepare($query); 

$params = array(
    ':book_id' => intval($_GET['book_id'])

);

// 3. execute query
$stmt->execute($params); 

// 4. display query 
// fetchall 
$book = $stmt->fetch(PDO::FETCH_ASSOC); 


?><!doctype html> 
<html lang= "en">
  <head> 
    <title><?=$book['title']?></title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
  
  <h1>Book Detail: <?=$book['title']?></h1>
  
  <?php foreach($book as $key => $value) : ?>
      
    <?php if($key != 'book_id') : ?>
      <li><strong><?=esc( label($key) )?></strong><?=esc( $value )?></li>
    <?php endif; ?>
  
  <?php endforeach; ?>
   
   
    
  </body>
</html>


