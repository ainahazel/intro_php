<?php

/**
 * Handle Add Publisher Form
 */

require __DIR__ . '/../../config.php';

// 1. Make sure it's a POST request, or die with a message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// if we reach this point, it is a POST request

// 2. Create an empty $errors array to hold our error messages
$errors = [];


// 3. Validate your form fields

if(empty($_POST['name'])) {
    $errors['name'] = 'Publisher Name is a required field.';
} elseif(strlen($_POST['name']) < 2 ) {
    $errors['name'] = 'Publisher name should atleast be two characters'; 
}

if(empty($_POST['city'])) {
    $errors['city'] = 'Publisher City is a required field.';
} 

if(empty($_POST['phone'])) {
    $errors['phone'] = 'Publisher Phone is a required field';
}


// 4. check to see if we have any errors

if(count($errors) > 0) {
    // Copy $errors array to SESSION so it can be
    // accessed on other pages
    $_SESSION['errors'] = $errors;
    // Copy POST values to SESSION so they can be
    // accessed other pages
    $_SESSION['post'] = $_POST;
    // Redirect to the form
    header("Location: 04_add_publisher_form.php");
    // if you do a header redirect, you MUST die on the next line
    die;
}

// 5.  If we get this far, the form was submitted without errors

// We want to create a new Publisher record in the database



// Create query
$query = 'INSERT INTO publisher 
            (name, city, phone)
            VALUES 
            (:name, :city, :phone)';

// Prepare the query
$stmt = $dbh->prepare($query);

// Binding our params
$params = array(
    ':name' => $_POST['name'],
    ':city' => $_POST['city'],
    ':phone' => $_POST['phone']
);

// Execute the query
$stmt->execute($params);


// 6. Redirect to thankyou page if insert was successful

$publisher_id = $dbh->lastInsertId();

if($publisher_id > 0) {
    header('Location: 06_success.php?publisher_id=' . $publisher_id);
    die;
}






