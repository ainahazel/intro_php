<?php

require __DIR__ . '/../../config.php';

// If there's no publisher_id, die, because we need
// the publisher_id to select that record from the DB
if(empty($_GET['publisher_id'])) {
    die('Please insert a publisher to see this page');
}

// By binding values to placeholders, we escape the
// data that is being bound, and protect ourselves
// from SQL injection attacks.
$query = "SELECT *
            FROM publisher
            WHERE 
            publisher_id = :publisher_id";

$stmt = $dbh->prepare($query);

$params = array(
    ':publisher_id' => $_GET['publisher_id']
);

$stmt->execute($params);

$result = $stmt->fetch();

// OR you can do this $result = $stmt->fetch(PDO::FETCH_ASSOC);

//dd($result);

?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title><?=$result['name']?></title>
    <meta charset="utf-8" />
      <style>
      </style>
  </head>
  <body>
   
        <h1> You added the following publisher </h1>
        
        
        <ul>
            <li><strong>Publisher ID:</strong> <?=$result['publisher_id']?> </li>
            <li><strong>Publisher Name:</strong> <?=$result['name']?> </li>
            <li><strong>Publisher City:</strong> <?=$result['city']?> </li>
            <li><strong>Publisher Phone:</strong> <?=$result['phone']?> </li>
            
        </ul>
   
       <p><a href="04_add_publisher_form.php">Add another publisher</a></p>
   
    
  </body>
</html>