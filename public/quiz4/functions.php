<?php

/**
 * Dump and Die - vardump a variable then die
 * @param  Mixed $var Variable to dump
 * @return [type]      [description]
 */
function dd($var)
{
    echo '<pre>';

    var_dump($var);

    echo '</pre>';

    // die;
    
}

/**
 * Escape potentially tainted string
 * @param  String $string 
 * @return String
 */
function esc($string)
{
    return htmlentities($string, null, "UTF-8");
}

function esc_attr($string)
{
    return htmlentities($string, ENT_QUOTES, "UTF-8");
}

/**
 * Get old value from array (eg post) for output to form
 * @param  String $field field name
 * @param  Array $post The array to get the field value from
 * @return String  The field value
 */
function old($field, $post)
{
    if(!empty($post[$field])) {
        return $post[$field];
    } else {
        return '';
    }
}

/**
 * Test if $_POST sub array contains a value
 * @param  String $value the needle
 * @param  String $array name of the nested array
 * @param  Array $post  $_POST
 * @return String 
 */
function checked($value, $array, $post)
{
    if(!empty($post[$array])) {
        $sub = $post[$array];
        if(in_array($value, $sub)) {
            return " checked ";
        }
    }
}
