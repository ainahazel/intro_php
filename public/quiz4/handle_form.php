<?php

/*
    
    Aina 
    Intro PHP Quiz #4
    April 24, 2020

*/



// start session 
session_start(); 


$interests = 'interests[]'; 

// Make sure it's a POST request, or die with a message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// add empty array to store errors
$errors = [];


// name is req
if(empty($_POST['name'])) {
    $errors['name'] = 'Name is a required field';
}

// validate name to be more than 4 chars 
if(strlen($_POST['name']) < 4) {
    $errors['name'] = 'Name must have atleast four characters';
}

// age is req 
if(empty($_POST['age'])) {
    $errors['age'] = 'Age is a required field';
}

// validate age 
if(!is_numeric($_POST['age'])) {
    $errors['age'] = 'Age must be a number';
}

// validate empty email
if(empty($_POST['email'])) {
    $errors['email'] = 'Email is a required field';
}

// validate email 
$email = $_POST['email'];

if(filter_var($email, FILTER_VALIDATE_EMAIL) !== $email) {
    $errors[$email] = ' invalid email <br/>'; 
}

// select at least two 
if(count($_POST['interests']) < 2) {
    $errors['interests[]'] = 'Select atleast two interests <br/>';  
}


// test for errors 
if(count($errors) > 0) {
//    // Copy $errors array to SESSION so it can be
//    // accessed on other pages
    $_SESSION['errors'] = $errors;
//    // Copy POST values to SESSION so they can be
//    // accessed other pages
    $_SESSION['post'] = $_POST;
//    // Redirect to the form
    header("Location: index.php");
//    // if you do a header redirect, you MUST die on the next line
    die;
} else {
    echo 'submission successful'; 
    die; 
}
//













