<?php

session_start();

require __DIR__ . '/functions.php';

$title = "Register";

// Retrieve $errors from session
if(isset($_SESSION['errors'])) {
	$errors = $_SESSION['errors'];
	$_SESSION['errors'] = [];
} else {
	$errors = [];
}

// Retrieve $post from session
if(isset($_SESSION['post'])) {
	$post = $_SESSION['post'];
	$_SESSION['post'] = [];
} else {
	$_SESSION['post'] = [];
}


?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Form Validation</title>
	<style>
		.required:before {
			content: "*";
			padding-right: 5px;
			color: #900;
			font-weight: 600;
		}
		.errors ul {
			padding: 0;
			margin: 0;
			list-style-type: none;
		}

		.errors {
			padding: 10px;
			display: inline-block;
			color: #900;
			border: 1px solid #900;
			background: #f7dbda;
		}
	</style>
</head>
<body>

<h1><?=$title?></h1>

<form action="handle_form.php" method="post" novalidate>

	<fieldset>
		<legend>Quiz 4 - Practical</legend>

		<?php if(!empty($errors)) : ?>
			<div class="errors"><ul>
			<strong>Please correct the following errors:</strong><br />
			<?php foreach($errors as $error) : ?>
				<li><?=$error?></li>
			<?php endforeach; ?>
			</ul></div>
		<?php endif; ?>

		<p class="required">Required fields marked with red asterisk</p>

		<p>
			<label class="required" for="name">Name</label><br />
			<input 
				type="text" 
				name="name"
				value="<?=esc_attr(old('name', $post))?>"
			/>
		</p>

		<p>
			<label class="required" for="age">Age</label><br />
			<input 
				type="text" 
				name="age"
				value="<?=esc_attr(old('age', $post))?>"
			/>
		</p>

		<p>
			<label class="required" for="email">Email</label><br />
			<input 
				type="text" 
				name="email"
				value="<?=esc_attr(old('email', $post))?>"
			/>
		</p>

		<p>
			<label class="required" for="interests[]">Interests</label><br />
			<input 
				type="checkbox" 
				name="interests[]"
				value="python"
				<?=checked('python', 'interests', $post)?>
			/> PHP<br />
			<input 
				type="checkbox" 
				name="interests[]"
				value="php"
				<?=checked('php', 'interests', $post)?>
			/> Python<br />
			<input 
				type="checkbox" 
				name="interests[]"
				value="ruby"
				<?=checked('ruby', 'interests', $post)?>
			/> Ruby<br />
			<input 
				type="checkbox" 
				name="interests[]"
				value="java"
				<?=checked('java', 'interests', $post)?>
			/> Java<br />
			<input 
				type="checkbox" 
				name="interests[]"
				value="c++"
				<?=checked('c++', 'interests', $post)?>
			/> C++<br />
		</p>
		<p>
			<button type="submit">Submit</button>
		</p>

	</fieldset>

</form>

</body>
</html>
























