<?php

require __DIR__ . "../variables.php"; 


/*
    Aina Ramos
    Intro to PHP
    2020-04-22
*/
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf8" />
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />

</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-sm-12">

            <h1><?=$title?></h1>

        </div>
    </div>

    <div class="row">


        <div class="col-sm-12">

            <table class="table table-striped">

                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Year Published</th>
                    <th>Price</th>
                    <th>Genre</th>
                </tr>


                <!-- Single Record Begins Here -->
                <?php foreach($result as $key => $value) : ?>
                <tr>
                    <td><?=$value['title']?></td>
                    <td><?=$value['author']?></td>
                    <td><?=$value['year_published']?></td>
                    <td><?=$value['price']?></td>
                    <td><?=$value['genre']?></td>
                </tr>
                <!-- Single Record Ends Here -->
                <?php endforeach;?>
            </table>


        </div>


    </div>

</div>

</body>
</html>