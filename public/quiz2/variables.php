<?php


$title = 'Books in Print';

$result = array (
  0 =>
  array (
    'title' => 'A Day in the Life',
    'year_published' => '2012',
    'price' => '22.99',
    'in_print' => '1',
    'author' => 'Carmen Ynez',
    'genre' => 'Literature',
  ),
  1 =>
  array (
    'title' => 'A Mixed Blessing',
    'year_published' => '2002',
    'price' => '12.99',
    'in_print' => '1',
    'author' => 'Sally Unger',
    'genre' => 'Politics',
  ),
  2 =>
  array (
    'title' => 'Carpet Baggers',
    'year_published' => '1977',
    'price' => '3.99',
    'in_print' => '0',
    'author' => 'Lee Sheldon',
    'genre' => 'Drama',
  ),
  3 =>
  array (
    'title' => 'Carrie',
    'year_published' => '1975',
    'price' => '4.99',
    'in_print' => '1',
    'author' => 'Stephen King',
    'genre' => 'Horror',
  ),
  4 =>
  array (
    'title' => 'Castle of Adventure',
    'year_published' => '1944',
    'price' => '33.99',
    'in_print' => '1',
    'author' => 'Enid Blyton',
    'genre' => 'SF',
  ),
  5 =>
  array (
    'title' => 'Caves of Steel',
    'year_published' => '1957',
    'price' => '4.99',
    'in_print' => '1',
    'author' => 'Isaac Asimov',
    'genre' => 'SF',
  ),
  6 =>
  array (
    'title' => 'Dune',
    'year_published' => '1975',
    'price' => '5.99',
    'in_print' => '0',
    'author' => 'Frank Herbert',
    'genre' => 'SF',
  ),
  7 =>
  array (
    'title' => 'Dune Messiah',
    'year_published' => '1977',
    'price' => '2.99',
    'in_print' => '1',
    'author' => 'Frank Herbert',
    'genre' => 'SF',
  ),
  8 =>
  array (
    'title' => 'Flash Forward',
    'year_published' => '2006',
    'price' => '19.99',
    'in_print' => '1',
    'author' => 'Robert Sawyer',
    'genre' => 'SF',
  ),
  9 =>
  array (
    'title' => 'Island',
    'year_published' => '2002',
    'price' => '4.99',
    'in_print' => '1',
    'author' => 'Richard Laymon',
    'genre' => 'Horror',
  ),
  10 =>
  array (
    'title' => 'Not a Penny More',
    'year_published' => '1980',
    'price' => '5.99',
    'in_print' => '1',
    'author' => 'Daniel Chambers',
    'genre' => 'Politics',
  ),
  11 =>
  array (
    'title' => 'The Oath',
    'year_published' => '2008',
    'price' => '24.99',
    'in_print' => '0',
    'author' => 'John Lescroart',
    'genre' => 'Legal',
  ),
  12 =>
  array (
    'title' => 'Under the Dome',
    'year_published' => '2010',
    'price' => '17.99',
    'in_print' => '1',
    'author' => 'Stephen King',
    'genre' => 'Horror',
  ),
);
