<?php 

require __DIR__ . '/../../config.php';


$users = array(
    'Davey', 
    'Shuba', 
    'Iqbal',
    'Tess',
    'Paul',
    'Frank',
    'Peter'
);

//dd($users);

// for loop

for($i=0; $i < sizeof($users); $i++) {
    echo $users[$i] . '<br/>'; 
}

echo '---------------------------------------------------------<br/>';
// for-each loop
// key and value is convention (key value pairs)
foreach($users as $key => $value) {
    echo $key . ':' . $value . '<br/>'; 
}

echo '---------------------------------------------------------<br/>';


//while loop
// remember you always have to declare its value and write its increment

$i = 0; 
while($i < sizeof($users)) {
    echo $users[$i] . '<br />'; 
    $i++; 
}


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        
      </style>
  </head>
    
  <body>
   
  <h1>Three Kinds of Loops</h1>
   
    <h2>For Loop</h2>
    <?php for($i=0; $i < sizeof($users); $i++) : ?>
       <li> <?=$users[$i]?> </li> <br/>
    
    <?php endfor; ?>
    
    

    <h2>For-each Loop</h2>
    
    <?php foreach($users as $key => $value) : ?> 
        <?=$key?> : <?=$value ?> <br /> 
    
    <?php endforeach; ?>
    
    
    
     
    <h2>While Loop</h2> 
    
    <?php $i = 0; while($i < sizeof($users)) : ?>
        <?=$users[$i] . '<br/>' ?>
        <?php  $i++; ?>
    <?php endwhile; ?>      
         
    

  </body>
</html>