<?php 


require __DIR__ . '/../../config.php';


// create an empty array 
// naturally indexed array 
$results = []; 


// add first book to results array 
$results[] = array(
    'id' => 1, 
    'title' => 'Dune',
    'num_pages' => 575,
    'price' => 7.99
);


$results[] = array(
    'id' => 2, 
    'title' => 'The Shining',
    'num_pages' => 650,
    'price' => 3.99
);

$results[] = array(
    'id' => 3, 
    'title' => 'Harry Potter',
    'num_pages' => 800,
    'price' => 10.99
);

$results[] = array(
    'id' => 4, 
    'title' => 'The Island',
    'num_pages' => 2000,
    'price' => 14.99
);



dd($results); 



?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body>
    <h1>Handling a Multi-dimensional Array</h1>
    <h2>Old school template</h2>
     
     <?php 
      
      /*
        This is the old way of doing the for-each loop 
      */
      
      foreach($results as $book) { // start of for-each
      
         echo "
         <div class=\'item\'>  
             <p>
             <strong>Title</strong>: {$book['title']}<br/>
             <strong>Number of Pages</strong>: {$book['num_pages']} <br/>
             <strong>Price</strong>: \$ {$book['price']}<br /> 
             <a href=\"readmore.php\">Read more&gt; &gt;</a>
             </p>
         </div>

         "; 
      } // end of for-each
      
     ?> 
      
    <h2>New School template</h2>
     
     <?php foreach($results as $book) : ?>
     
     <!-- Repeat for each book-->
     <div class='item'>  
             <p>
             <strong>Title</strong>: <?=$book['title']?> <br/>
             <strong>Number of Pages</strong>: <?=$book['num_pages']?> <br/>
             <strong>Price</strong>: $ <?=$book['price']?><br /> 
             <a href="readmore.php">Read more&gt; &gt;</a>
             </p>
         </div>
         
     <!-- End for each -->  
      <?php endforeach; ?> 
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
  </body>
</html>