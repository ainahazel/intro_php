<?php 

require __DIR__ . '/../../config.php'; 


$users = [];

$users[] = array(
    'id' => 1, 
    'email' => 'aina@haha.com', 
    'first' => 'Aina',
    'last' => 'Ramos',
    'is_active' => 1
);

$users[] = array(
    'id' => 2, 
    'email' => 'dave@haha.com', 
    'first' => 'Davey',
    'last' => 'Jones',
    'is_active' => 1
);

$users[] = array(
    'id' => 3, 
    'email' => 'aina@haha.com', 
    'first' => 'Kaeziah',
    'last' => 'Bobier',
    'is_active' => 1
);

$users[] = array(
    'id' => 4, 
    'email' => 'Steve@haha.com', 
    'first' => 'Stevie',
    'last' => 'Wonder',
    'is_active' => 0
);

$users[] = array(
    'id' => 5, 
    'email' => 'captain@haha.com', 
    'first' => 'Jack',
    'last' => 'Sparrow',
    'is_active' => 1
);

//dd($users); 

// users is a mock db results 


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
          
          
          .user {
              border: 1px solid #cfcfcf; 
              margin-bottom: 20px; 
          }
          
          .user.inactive {
              background: red; 
              color: white; 
              border: 2px solid black;
          }
          
      </style>
  </head>
  <body>
  
  
   <h1>Template Syntax</h1>
      
      <!-- Step 1: hard code what a single record would look like -->
      <!-- Step : create the for-each looop around the hard-codded record -->
      <!-- Step 3: Replace hard-coded values with variables -->
      
      <!--
      
      Note: aina, the $row is what you will refer too in the actual php code - so you could name it any name - doesn't have to be row. 
      
      You could have named it users, but when you think of it logically, each row is each user
        
       -->
      
      
      
      
    <?php foreach($users as $row) : ?>
       
       
        <?php if($row['is_active'] == 1) : ?> 
       
            <div class="user">
                <ul>
                    <li><strong>Name:</strong> <?= $row['first']?>,<?=$row['last']?> </li>
                    <li><strong>Email:</strong> <?= $row['email']?></li>

                </ul>
            </div>
      
        <?php endif; ?> 
      
    <?php endforeach; ?>
     
     
    <h2>Template Syntax with ELSE</h2>
     
    <?php foreach($users as $row) : ?>
       
       
        <?php if($row['is_active'] == 1) : ?> 
       
            <div class="user">
                <ul>
                    <li><strong>Name:</strong> <?= $row['first']?>,<?=$row['last']?> </li>
                    <li><strong>Email:</strong> <?= $row['email']?></li>

                </ul>
            </div>
      
        <?php else : ?> 
           
           <div class="user inactive">
                <ul>
                    <li><strong>Name:</strong> <?= $row['first']?>,<?=$row['last']?> </li>
                    <li><strong>Email:</strong> <?= $row['email']?></li>

                </ul>
            </div>
            
        
        <?php endif; ?>
      
    <?php endforeach; ?>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
  </body>
</html>