<?php 

// this is a relative path 
    //require '../../config.php';


// this is an absolute path
__DIR__ require '../../config.php';
   



// three ways to create an array 

// 1. regular ordinal array - don't need a new 
$fruits = array('orange', 'apple', 'banana');

// 2. literal array

$fruits2 = ['banan', 'kiwi', 'pineapple'];

// 3. this is the equivalent of doing 'push'

// in JS it looks like this: 
// fruits3.push('strawberry);
// fruits3.push('lemon');

$fruits3 = '';

// this is the actual array in php 
$fruits3[] = 'orange'; 
$fruits3[] = 'blueberry'; 
$fruits3[] = 'banana'; 

dd($fruits); 


// -----------------------------

$user = [];

$user['firstname' => 'Dave', 'last_name', 'Brown']






?> 