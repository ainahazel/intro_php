<?php 

/*
    Conditionals in  PHP 
*/


// IF 

// IF ELSE 

// IF ELSEIF ELSE 

// JAVASCRIPT 
// IF (CONDITION) {
//      DO SOMETHING
// } ELSE IF (ANOTHER CONDITION) {
//      DO SOMETHING ELSE 
// }   ELSE {
// DEFAULT
// }


// PHP 
// IF (CONDITION) {
//      DO SOMETHING
// } ELSE IF (ANOTHER CONDITION) {
//      DO SOMETHING ELSE 
// }   ELSE {
// DEFAULT
// }


$users = ['Davey', 'Sally'];

$fruits = []; 

$name1 = 'Aina';

$name2 = ''; 

// test to see if theres something in it 
// truthiness.. 

if($users) {
    echo $users[0] . '<br/>'; 
} else {
    echo 'Nothin in this array'; 
}
// output: Davey = truthy



// testing falsiness 


if($fruits) {
    echo $fruits[0] . '<br/>'; 
} else {
    echo 'Nothin in this array <br/> '; 
}

//output : 'Nothin in this array'


## true boolean test for users 

if( sizeof($users) > 0 ) {
    echo $users[0] . '<br/>'; 
} else {
    echo 'Nothin in this array'; 
}


## true boolean test for fruits 

if( sizeof($fruits) > 0 ) {
    echo $fruits[0] . '<br/>'; 
} else {
    echo 'Nothin in this array <br/>'; 
}

## you can use strlen too 

if( strlen($name1) > 0 ) {
    echo $name1 . '<br/>'; 
} else {
    echo 'Nothin in this array <br/>'; 
}

## you can use strlen too 

if( strlen($name2) > 0 ) {
    echo $name2 . '<br/>'; 
} else {
    echo 'Nothin in this string'; 
}












