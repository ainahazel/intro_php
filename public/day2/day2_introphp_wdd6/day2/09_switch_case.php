<?php 

// Find 5, 10, or 20

$x = 10;


switch($x) {

    case 5:
        echo 'less than 10 <br />';
        break;
    case 10:
        echo 'Equal to 10! <br />';
        break;
    case 20:
        echo 'Greater than 10 <br />';
        break;
    default:
       

}

// Both sets of code are functionally identical

if($x == 5) {
    echo 'less than 10 <br />';
} elseif($x == 10) {
    echo 'Equal to 10! <br />';
} elseif($x == 20) {
    echo 'Greater than 10 <br />';
} else {
     echo 'Not my number <br />';
}

