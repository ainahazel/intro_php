<?php

// || -- OR
// && -- AND
// ! -- NOT

$a = true;
$b = false;


if($a || $b); // true
if($a && $b); // false
if(!$a); // false
if(!$b); // true

