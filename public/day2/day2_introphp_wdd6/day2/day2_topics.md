# Day 2

9:00 AM to 10:30 AM

* Quiz

* Data Types and Dynamic Typing
    - Numbers
        + Integers
        + Floats
    - Strings
    - Boolean
    - Null
    - Arrays
    - Objects
    - Resources

* Conditionals
    - if elseif else
    - switch case
    - Truthy/Falsey

* Loops
    - For loop
    - While loop
    - Foreach loop

* Imports
    - include
    - require
    - include_once
    - require_once
    - Building a mini-site with includes
    - Adding breadcrumbs using conditionals

* Forms
    - Submitting forms
    - Processing FORM data
    - Accessing Form Data
        + GET SuperGlobal
        + POST SuperGlobal

* Escaping for Output
    - Escaping for Context
        - in HTML 
        - in Attribute
    - Our own escaping functions (esc, esc_attr)

