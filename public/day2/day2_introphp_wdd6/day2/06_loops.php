<?php

require __DIR__ . '/../../config.php';

$users =  array(
    'Davey',
    'Shuba',
    'Iqbal',
    'Tess',
    'Paul',
    'Frank',
    'Peter'
);

// dd($users);

/*

// For loop
for($i=0; $i<sizeof($users); $i++) {
    echo $users[$i] . '<br />';
}

echo '<hr />';

// Foreach loop allows to access the key and the value
foreach($users as $key => $value) {
    echo $key . ': ' . $value . '<br />';
}

echo '<hr />';

// While loop -- works exactly like a for loop
$i = 0;
while($i<sizeof($users)) {
    echo $users[$i] . '<br />';
    $i++;
}

echo '<hr />';

// Dowhile loop - same as While, except always executes at least once
$i = 0;
do {
    echo $users[$i] . '<br />';
    $i++;
} while($i<sizeof($users))

*/

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Loops</title>
</head>
<body>

    <h1>Three Loops</h1>

    <h2>For Loop</h2>

    <?php for($i=0; $i<sizeof($users);$i++) : ?>
        <?=$users[$i]?> <br />
    <?php endfor; ?>

    <h2>Foreach Loop</h2>

    <?php foreach($users as $key => $value) : ?>
        <?=$key?> : <?=$value?> <br />
    <?php endforeach; ?>

    <h2>While Loop</h2>

    <?php $i=0; while($i<sizeof($users)) : ?>
        <?=$users[$i]?> <br />
        <?php $i++ ?>
    <?php endwhile; ?>


    <!-- Can't use DO WHILE in alternative syntax -->

</body>
</html>