<?php

/*
Arrays in PHP
 */

// relative path
// require '../../config.php';

// absolute path
require __DIR__ . '/../../config.php';

$title = "Arrays in PHP";

// __PATH__
// __FILE__

// echo __DIR__; // D:\htdocs\intro_php\wdd6\public\day2

// Arrays in PHP are very similar to arrays in Javascript.
// However, keep in mind, that arrays are NOT objects in PHP

// Three ways to create an array

// in JS: var fruits = new Array('apple', 'orange', 'banana');
$fruits = array('apple', 'orange', 'banana');

// in JS:  var fruits2 = ['mango', 'banana', 'peach'];
$fruits2 = ['banana', 'mango', 'peach'];

// We can built the array one value at a time
// in JS:  var fruits3 = [];
// fruits3.push('orange');
// fruits3.push('lemon');
// fruits3.push('lime');
$fruits3 = [];

// actual push in PHP
// array_push($fruits3, 'orange');

// shorthand for push
$fruits3[] = 'orange';
$fruits3[] = 'lemon';
$fruits3[] = 'lime';

// dd($fruits);


// Associative arrays in PHP are arrays (not objects)
// We use the array assignment operator to assign values
// within them
$user = [
        'first_name' => 'Dave', 
        'last_name' => 'jones', 
        'age' => 22
    ];

// IN JS:
// var user = {
//      first_name: 'Dave',
//      last_name: 'Jones',
//      age: 22
// }
// 


// Create an indexed array with 6 colors

$colors = array(
    'red',
    'green',
    'blue'
);

// Create an associative array for a book
//  -- id
//  -- title
//  -- num_pages
//  -- price (float)
//  -- year_pubished
//  -- in_print (boolean)

$book = array(
    'id' => 12,
    'title' => 'Dune',
    'num_pages' => 575,
    'price' => 7.99,
    'year_published' => 1975,
    'in_print' => true
);


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
</head>
<body>

    <h1><?=$title?></h1>

    <h2>Colors</h2>

    <ul>
    <?php
        // in JS: for(var i=0;i<colors.length;i++) {
        //     document.write('<li>' + colors[i] + '</li>');
        //     document.write(`<li>{colors[i]}</li>`); // not Brent friendly
        // }
        for($i=0;$i<count($colors);$i++) {
            echo "<li>{$colors[$i]}</li>\n";
            // echo "<li>" . $colors[$i] . "</li>";
        }

    ?>
    </ul>

    <h2>Book</h2>

    <ul>
    <?php
    /*
        In JS: we use a for in loop
        for(var i in book) {
            console.log(i); // the key
            console.log(book[i]);  // value
        }
    */
    // In PHP we use a foreach loop
    
    // foreach($array_name as $key => $value)
    foreach($book as $key => $value) {
        echo "<li><strong>{$key}</strong>: {$value}</li>\n";
    }


   ?>
    </ul>


    <h2>Colors 2</h2>
    <h3>Without the key</h3>

    <ul>
        <?php
            foreach($colors as $value) {
                echo "<li>{$value}</li>\n";
            }
        ?>
    </ul>

</body>
</html>