<?php

require __DIR__ . '/../../config.php';

$users = [];

$users[] = array(
    'id' => 1,
    'email' => 'djones@example.com',
    'first' => 'Davey',
    'last' => 'Jones',
    'is_active' => 1
);

$users[] = array(
    'id' => 2,
    'email' => 'msirus@example.com',
    'first' => 'Miley',
    'last' => 'Sirus',
    'is_active' => 1
);

$users[] = array(
    'id' => 3,
    'email' => 'mjagger@example.com',
    'first' => 'Mick',
    'last' => 'Jagger',
    'is_active' => 1
);

$users[] = array(
    'id' => 4,
    'email' => 'lreed@example.com',
    'first' => 'Lou',
    'last' => 'Reed',
    'is_active' => 0
);

$users[] = array(
    'id' => 5,
    'email' => 'fherbert@example.com',
    'first' => 'Frank',
    'last' => 'Herbert',
    'is_active' => 1
);


// dd($users);
// $users is a mock database result


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Alternative Syntax Review</title>
</head>
<body>

    <h1>Alternative PHP Syntax Review</h1>


    <!-- Step One -- Hardcode desired output for one record -->
    <!-- Step Two -- Place loop around hardcoded record -- test -->
    <!-- Step Three -- Replace hard-coded values with variables -->

    <?php foreach($users as $row) : ?>

        <div>
            <ul>
                <li><strong>ID</strong>: <?=$row['id']?></li>
                <li><strong>First Name</strong>: <?=$row['first']?></li>
                <li><strong>Last Name</strong>: <?=$row['last']?></li>
                <li><strong>Email:</strong>: <?=$row['email']?></li>
            </ul>
        </div>

    <?php endforeach; ?>

</body>
</html>