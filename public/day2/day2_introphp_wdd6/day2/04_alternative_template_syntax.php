<?php

require __DIR__ . '/../../config.php';

$users = [];

$users[] = array(
    'id' => 1,
    'email' => 'djones@example.com',
    'first' => 'Davey',
    'last' => 'Jones',
    'is_active' => 1
);

$users[] = array(
    'id' => 2,
    'email' => 'msirus@example.com',
    'first' => 'Miley',
    'last' => 'Sirus',
    'is_active' => 1
);

$users[] = array(
    'id' => 3,
    'email' => 'mjagger@example.com',
    'first' => 'Mick',
    'last' => 'Jagger',
    'is_active' => 1
);

$users[] = array(
    'id' => 4,
    'email' => 'lreed@example.com',
    'first' => 'Lou',
    'last' => 'Reed',
    'is_active' => 0
);

$users[] = array(
    'id' => 5,
    'email' => 'fherbert@example.com',
    'first' => 'Frank',
    'last' => 'Herbert',
    'is_active' => 1
);


// dd($users);
// $users is a mock database result


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Template Syntax</title>

    <style>

        .user {
            border: 1px solid #cfcfcf;
            padding: 20px;
            margin-bottom: 20px;
        }

        .user.inactive {
            background: #F00;
            color: #FFF;
            border: 2px solid #000;
        }

    </style>
</head>
<body>

    <h1>Template Syntax</h1>

    <!-- Step 1: Hard code what a single record will look like -->
    <!-- Step 2: Create the foreach loop around the hard-coded record (and test) -->
    <!-- Step 3: Replace hard-coded values with variables -->

    <?php foreach($users as $row) : ?>

        <?php if($row['is_active'] == 1) : ?>

            <div class="user">
                <ul>
                    <li><strong>Name</strong>: <?=$row['first']?> <?=$row['last']?></li>
                    <li><strong>Email</strong>: <?=$row['email']?></li>
                </ul>
            </div><!-- /item -->

        <?php endif; ?>

    <?php endforeach; ?>



    <h2>Template Syntax with else</h2>

     <?php foreach($users as $row) : ?>

        <?php if($row['is_active'] == 1) : ?>

            <div class="user">
                <ul>
                    <li><strong>Name</strong>: <?=$row['first']?> <?=$row['last']?></li>
                    <li><strong>Email</strong>: <?=$row['email']?></li>
                </ul>
            </div><!-- /item -->

        <?php else : ?>

            <div class="user inactive">
                <ul>
                    <li><strong>Name</strong>: <?=$row['first']?> <?=$row['last']?></li>
                    <li><strong>Email</strong>: <?=$row['email']?></li>
                </ul>
            </div><!-- /item -->

        <?php endif; ?>

    <?php endforeach; ?>

</body>
</html>