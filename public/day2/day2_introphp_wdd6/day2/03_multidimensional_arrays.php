<?php

/*
MultiDimensional Arrays
 */

require  __DIR__ . '/../../config.php';

// Create an empty array
// naturally indexed array
$results = [];

// Push first book on to results array
// each book is an associative array
$results[] = array(
    'id' => 1,
    'title' => 'Dune',
    'num_pages' => 575,
    'price' => 7.99
);

// Push second book on to results array
$results[] = array(
    'id' => 2,
    'title' => 'The Shining',
    'num_pages' => 650,
    'price' => 12.99
);

// Push third book on to results array
$results[] = array(
    'id' => 3,
    'title' => 'The Island',
    'num_pages' => 322,
    'price' => 4.99
);

// Push fourth book on to results array
$results[] = array(
    'id' => 4,
    'title' => 'The Chalk Man',
    'num_pages' => 322,
    'price' => 11.99
);


// dd($results);

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>MultiDimensional Arrays</title>
    <style>

        body {
            font-family: "Open Sans", Verdana, sans-serif;
        }

        .item {
            padding: 20px;
            border: 1px solid #cfcfcf;
            margin-bottom: 20px;
        }

    </style>
</head>
<body>

    <h1>Handling a Multidimensional Array</h1>

    <h2>Old School Template</h2>

    <?php

    foreach($results as $book) {

        // Old School, standard syntax

        echo "
        
        <!-- Repeat for every book -->
        <div class=\"item\">

            <p><strong>Title</strong>: {$book['title']}<br />
                <strong>Number of pages</strong>: {$book['num_pages']}<br />
                <strong>Price</strong> \${$book['price']}<br />
                <a href=\"readmore.php\">Read more&gt;&gt;</a></p>

        </div>
        <!-- end book -->

        ";

    } // end foreach

    ?>


    <h2>New School Template</h2>


    <?php foreach($results as $book) : ?>

        <!-- Repeat for every book -->
        <div class="item">

            <p><strong>Title</strong>: <?=$book['title']?><br />
                <strong>Number of pages</strong>: <?=$book['num_pages']?><br />
                <strong>Price</strong> $<?=$book['price']?><br />
                <a href="readmore.php">Read more&gt;&gt;</a></p>

        </div>
        <!-- end book -->

    <?php endforeach; ?>




</body>
</html>