<?php

/*
Conditionals in PHP
*/


// if

// if else

// if elseif else

// JS:
// if(condition) {
//      // do something
// } else if(another condition) {
//      // do something else
// } else {
//      // defaut
// }

// PHP
// if(condition) {
//      // do something
// } elseif(another condition) {
//      // do something else
// } else {
//      // defaut
// }

$users = ['davey', 'sally']; // truthy

$fruits = []; // falsey

$name1 = 'Harry'; // truthy

$name2 = ''; // falsey

// Truthiness
if($users) {
    echo $users[0] . '<br />';
} else {
    echo 'Nothing in this array<br />';
}

echo '<hr />';

// falsiness
if($fruits) {
    echo $fruits[0] . '<br />';
} else {
    echo 'Nothing in this array<br />';
}


echo '<hr />';

// Boolean true
if(sizeof($users) > 0) {
    echo $users[0] . '<br />';
} else {
    echo 'Nothing in this array<br />';
}

echo '<hr />';

// Boolean false
if(sizeof($fruits) > 0) {
    echo $fruits[0] . '<br />';
} else {
    echo 'Nothing in this array<br />';
}


echo '<hr />';

// Tests for truthiness - truthy
if($name1) {
    echo 'There is a value in name1<br />';
} else {
    echo 'There is no value in name1<br />';
}

echo '<hr />';

// Tests for falsiness -- falsey
if($name2) {
    echo 'There is a value in name2<br />';
} else {
    echo 'There is no value in name2<br />';
}

echo '<hr />';

// Test for boolean -- true
if(strlen($name1) > 0) {
    echo 'There is a value in name1<br />';
} else {
    echo 'There is no value in name1<br />';
}


echo '<hr />';

// Test for boolean -- false
if(strlen($name2) > 0) {
    echo 'There is a value in name2<br />';
} else {
    echo 'There is no value in name2<br />';
}


// Always test for a real Boolean value 
// rather than simply for truthy or falsey values
// 
// This requires a few characters more code, but
// is always correct... it forces you to consider
// what makes the condition true or false, and
// that's something you should always know.