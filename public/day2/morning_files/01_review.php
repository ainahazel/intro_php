<?php

/*
Review of some Day 1 concepts
 */

// In string functions, always use the mb_ variant if ther is one.

$name = 'Ed';

echo strlen($name); // 2  .. strlen counts the number of bytes in a string

echo strpos($name, 'd'); // 1

$name = "Éd";

echo strlen($name); // 3


echo mb_strlen($name); // 2 // mb_strlen counts the number of characters

echo strpos($name, 'd'); // 2

mb_strpos($name, 'd'); // 1

/*

Output from PHP interactive shell:

php > $name = "Éd";
php > echo strlen($name);
3
php > echo strpos($name, 'd');
2
php > echo mb_strpos($name, 'd');
1
php >

*/



