# Day 3 Topics

## Form Processing

    * Form construction
        - use `name` attribut efor all form fields
        - use post method
        - use second file for processing as `action`
        - eg:

```html

    <form action="process_form.php" method="post">
        <p><label for="first_name">First name</label>
        <input type="text" name="first_name" /></p>
        <p><button type="submit">Submit</button></p>
    </form>

```
    * Form processing
    * Super Globals
        - `$_GET`
        - `$_POST`
        - `$_SERVER`
        - `$_SESSION`
        - `$_COOKIE`
        - `$_ENV`
    * Escaping output (xss, csrf, sqli)
        - FIEO
    * Validation (required)
    * PRG (Post Redirect Get design pattern)
        - Post request from form to form processing file
        - Redirect after processing to another page with a GET request
    

## Form Validation

Using the PHP Session and the Post Redirect Get (PRG) design pattern, follow these steps.

1. Make sure to start the session in your config file, and include the
config file in all files:

```php
    session_start();
```

2. In your `handle_form.php` file that handles the POST request, follow these steps.

* Include the `config.php` file at top of file to ensure your session is started
* Test to make sure the request is a POST request, or die with a message
* Create an empty errors array to hold your error messages
* Validate each form field, adding error messages to the errors array if required
* After you have finished validating:
    - Test to see if the `$errors` array is empty
    - If you have errors
        + Save `$errors` array to `$_SESSION['errors']`
        + Save `$_POST` to `$_SESSION['post']`
        + Do a header redirect back to the form
        + die

3. In your `form.php` file, follow these steps

* Include the `config.php` file at the top of file to ensure your session is started
* Before the form, in the HTML, test that `$_SESSION['errors']` is not empty
* If there are errors, loop through them and output them inside the if statement
* At the very bottom of the file, after the closing html tag, you need to clear the old errors and form values in a php block:

```php

if(isset($_SESSION['errors'])) {
    unset($_SESSION['errors']);
}
if(isset($_SESSION['post'])) {
    unset($_SESSION['post']);
}

```




