<?php

require __DIR__ . '/../../config.php';

// $_SERVER

// dd($_SERVER);

// Test to make sure we have a POST request
// else, die with an error message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// Available SuperGlobals
// $_GET - GET REQUESTS
// $_POST - POST REQUESTS

// GET requests, ask information form the server (eg a page)
// POST requests, send NEW information to the server, generally to create a record

// dd($_POST); 

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Thanks for Registering!</title>
</head>
<body>

    <h1>Thanks for registering!</h1>

    <h2>You provided the following information:</h2>

    <ul>
        <li><strong>Name</strong>: <?=$_POST['name']?></li>
        <li><strong>Email</strong>: <?=$_POST['email']?></li>
    </ul>

</body>
</html>