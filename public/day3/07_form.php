<?php

/*
PHP Form
 */

$title = "PHP Forms";

require __DIR__ . '/../../config.php';
// we have access to session

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
    <style>
        .required:before {
            content: "* ";
            color: #F00;
            font-weight: bold;
        }

        .errors {
            display: inline-block;
            padding: 0 10px;
            border: 1px solid #F00;
            color: #F00;
        }
    </style>
</head>
<body>

    <!-- Output $title in an h1 -->
    <h1><?=$title?></h1>

    <p class="required"><small>Required fields are marked with a red asterisk.</small></p>

    <!-- Create a simple form with two fields: name and email -->
    <!-- Action: 03_handle_form.php   Method:  post   novalidate -->

    <?php if(!empty($_SESSION['errors'])) : ?>
        <div class="errors">
            <ul>
                <?php foreach($_SESSION['errors'] as $error) : ?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <form action="08_handle_form.php" method="post" novalidate>
        <p><label class="required" for="name">Name: </label>
            <input type="text" name="name" id="name" value="" /></p>
        <p><label class="required" for="email">Email: </label>
            <input type="text" name="email" id="email" value="" /></p>
        <p><button type="submit">Submit</button></p>
    </form>


    <p>Passing data in the query string, appended to a URL: <a href="04_get_dump.php?book_id=12">Load Book Info</a></p>

</body>
</html>
<?php 

    // Middle ware execute AFTER page has processed
    
    // get rid of the errors
    if(isset($_SESSION['errors'])) {
        unset($_SESSION['errors']);
    }

    // get rid of old POST values
    if(isset($_SESSION['post'])) {
        unset($_SESSION['post']);
    }

?>