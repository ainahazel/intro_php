<?php

require __DIR__ . '/../../config.php';

// $_SERVER

// dd($_SERVER);
// 

$class='regi"ster';

// Test to make sure we have a POST request
// else, die with an error message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// Available SuperGlobals
// $_GET - GET REQUESTS
// $_POST - POST REQUESTS

// GET requests, ask information form the server (eg a page)
// POST requests, send NEW information to the server, generally to create a record

// dd($_POST); 

// Security Paradigm:  FIEO = Filter Input, Escape Output

// Rules of Thumb: 
// 1. Assume that all data is tainted
// 2. Assume that every request is an attack

// There are two functions we can use to handle the Escape Output part of FIEO
// htmlentities
// htmlspecialchars


// Escape for the context where we are using the data.
// 1. In HTML:  htmlentities($string, null, "UTF-8")
// 2. In Attrubute:  htmlentities($string, ENT_QUOTES, "UTF-8")
// 3. For insertion into database: binding our data


// FI = Filter Input
// FI = Validation


// isset() -- returns true, if the variable is declared

// empty() -- returns true in one two cases
// 1.  The variable doesn't exist
// 2.  It exists, but has an empty value (empty array, empty string)


// 1.  Create an empty errors array to hold any errors
// -- This will be a flag.  If it's got errors, it will
// have a count, and we can stop the script and output
// the errors
$errors = [];
// $errors = array();

// 2. Validate every field
// -- In this case, we are only validating to make sure
// that required fields are filled in.

// Name validators
if(empty($_POST['name'])) {
    $errors['name'] = 'Name is a required field';
} elseif(mb_strlen($_POST['name']) < 2) {
    $errors['name'] = 'Name must have at least 2 characters';
}

// Email Validators
if(empty($_POST['email'])) {
    $errors['email'] = 'Email is a required field';
} elseif(mb_strlen($_POST['email']) < 6) {
    $errors['email'] = 'Email must be at least 6 characters';
}

// dd($errors);

// 3. Check the errors array to see if any errors have
// been save in it.  If so output those errors... and die
// We have to die, so we don't load the HTML at the bottom
// of the page

if(count($errors) > 0) {

    echo '<h2>Please go back and correct these errors:</h2>';
    echo '<ul>';
    foreach($errors as $error) {
        echo "<li>{$error}</li>";
    }
    echo '</ul>';

    die;
}


// 4.  If we get to this point, that means there are no errors
// and every field has been filled in... so we can simply
// output the values to the page.


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Thanks for Registering!</title>
</head>
<body class="<?=esc_attr($class)?>">

    <h1>Thanks for registering!</h1>

    <h2>You provided the following information:</h2>

    <ul>
        <li><strong>Name</strong>: <?=esc($_POST['name'])?></li>
        <li><strong>Email</strong>: <?=esc($_POST['email'])?></li>
    </ul>

</body>
</html>