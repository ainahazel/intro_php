<?php

require __DIR__ . '/../../config.php';
// We now have a session here, too

// Test to make sure we have a POST request
// else, die with an error message
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method.');
}

// Empty array to handle our error messages
$errors = [];

// Name validators
if(empty($_POST['name'])) {
    $errors['name'] = 'Name is a required field';
} elseif(mb_strlen($_POST['name']) < 2) {
    $errors['name'] = 'Name must have at least 2 characters';
}

// Email Validators
if(empty($_POST['email'])) {
    $errors['email'] = 'Email is a required field';
} elseif(mb_strlen($_POST['email']) < 6) {
    $errors['email'] = 'Email must be at least 6 characters';
}

if(count($errors) > 0) {

    // Our error array must be saved into the $_SESSION
    // array so that it can be accessed on other pages
    $_SESSION['errors'] = $errors;
    // This will send form values into session, too
    $_SESSION['post'] = $_POST;
    // send browser back to form
    // with a header redirect
    header("Location: 07_form.php");
    die;
    // we die so that the browser doesn't fall through
    // an execute more code while it is in the process
    // of redirecting.
    // 
}