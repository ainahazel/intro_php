<?php 

require __DIR__ . "/../../config.php";

$title = "Form"; 


// PHP form 


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title><?=$title?></title>
    <meta charset="utf-8" />
      <style>
          .required:before {
              content: "* "; 
              color: red; 
              font-weight: bold; 
          }
        
      </style>
  </head>
    
  <body>
  
  <h1><?=$title?></h1>
  
  
  <?php if(!empty($_SESSION['errors']) > 0) : ?>

  <div class="errors">
      <ul>
          <?php foreach($_SESSION['errors'] as $error) : ?> 
              <li><?=$error?></li>
          
          <?php endforeach; ?> 
      
      </ul>
  </div>  
  
  <?php endif; ?> 
  
  <form action="08_handle_form.php" method="POST" novalidate>
  
  <p>Small asterisk means it is a required field</p>

  <p><label for="name" class=required>First name:</label>
      <input type="text" id="name" name="name" vaue=""></p>
  <p><label for="email" class=required>Email:</label>
          <input type="text" id="email" name="email" value=""></p>
          
  <p><button type="submit">Submit</button></p>
</form>
   
     
     
     
      
  </body>
</html>
<?php
//    unset($_SESSION['$errors']);
//    unset($_SESSION['post']); 

    if (isset($_SESSION['$errors'])) {
        unset($_SESSION['$errors']);
    }

    if (isset($_SESSION['post'])) {
        unset($_SESSION['post']);
    }



?>