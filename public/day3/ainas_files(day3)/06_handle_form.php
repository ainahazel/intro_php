<?php 

// $_SERVER 

require __DIR__ . "/../../config.php";

//require __DIR__ . "/../../functions.php";
//dd($_SERVER); 


$class= 'reg"ister'; 

// test to make sure you have a post request
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method'); 
}


# 1. make an array
$errors = [];

# 2. validate 
if(empty($_POST['name'])) {
    $errors['name'] = "Name is a required field";
} elseif(strlen($_POST['name']) < 2) {
    $errors['name'] = "Name must have at least 5 characters";
}

if(empty($_POST['email'])) {
    $errors['Email'] = "Email is a required field";
} elseif(strlen($_POST['email']) < 6) {
    $errors['email'] = "Name must have at least 5 characters";
}

//dd($errors); 

# 3. check errors array 

if(count($errors) > 0) {
    echo '<h2> Please go back and correct these errors </h2>';
    echo'<ul>';
    foreach($errors as $error) {
        echo "<li>{$error}</li>"; 
    }
    echo "</ul>";
    die; 
}

//dd($_POST);

?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title>About</title>
    <meta charset="utf-8" />
      <style>
        body{
          width: 100vw; 
        }
        
      </style>
  </head>
    
  <body class="<?=esc_atr($class)?>">
    <ul>
        <li><strong>Name:</strong>: <?=esc($_POST['name'])?></li>
        <li><strong>Email</strong>: <?=esc($_POST['email'])?></li>
    </ul>
      
  </body>
</html>