<?php 

require __DIR__ . "/../../config.php";

$title = "Form"; 


// PHP form 


?><!doctype html> 
     
<html lang= "en">
  <head> 
    <title><?=$title?></title>
    <meta charset="utf-8" />
      <style>
          .required:before {
              content: "* "; 
              color: red; 
              font-weight: bold; 
          }
        
      </style>
  </head>
    
  <body>
  
  <h1><?=$title?></h1>
  
  <form action="03_handle_form.php" method="POST">
  
  <p>Small asterisk means it is a required field</p>

  <p><label for="name" class=required>First name:</label>
      <input type="text" id="name" name="name" ></p>
      <p><label for="email" class=required>Email:</label>
          <input type="text" id="email" name="email"></p>
          
  <p><button type="submit">Submit</button></p>
</form>
   
     
     
     
      
  </body>
</html>