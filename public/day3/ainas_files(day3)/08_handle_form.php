<?php 

require __DIR__ . "/../../config.php"; 


if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Unsupported request method'); 
}


$errors = []; 

if(empty($_POST['name'])) {
    $errors['name'] = "Name is a required field";
} elseif(strlen($_POST['name']) < 2) {
    $errors['name'] = "Name must have at least 5 characters";
}

if(empty($_POST['email'])) {
    $errors['Email'] = "Email is a required field";
} elseif(strlen($_POST['email']) < 6) {
    $errors['email'] = "Name must have at least 5 characters";
}


if(count($errors) > 0) {
    
    // our error array must be saved into the session array so that it can be accessed on other pages 
    $_SESSION['$errors'] = $errors; 
    
    $_SESSION['post'] = $_POST; 
    
    // redirect browser back to form 
    // die right away so it doesn't execute everything else
    header("Location: 07_form.php"); 
    die; 
}
