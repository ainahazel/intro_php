<?php 



function dd($var) 
{
    echo '<pre>';
    
    var_dump($var); 
    
    echo '</pre>'; 
    
    die; 
}


/**
* escape potentially tainted string
* @param [string] 
* @return
*
*/
function esc($string) 
{
    return htmlentities($string, null, "UTF-8");
}

function esc_attr($string) 
{
    return htmlentities($string, EN_QUOTES, "UTF-8");
}

/**
* escape potentially tainted string
* @param [string] - string field name
* @param [array] - array to get the field value from 
* @return [string] - field value 
*
*/

// we want the name of the field and the array that it came from 
function old($field, $post) 
{
//    $post = $_SESSION['post'];
    if(!empty($post[$field])) {
        return $post[$field]; 
    } else {
        return ''; 
    }
}


function label($field) 
{
    // first_name to First Name 
    $label = str_replace('_', ' ', $field); 
    $label = ucwords($label); 
    
    
}



